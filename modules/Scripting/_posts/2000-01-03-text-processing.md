---
title: Text processing (using sed & awk)
---

## Table of contents
{:.no_toc}

* toc placeholder
{:toc}

## Slides

{% include slides.html id="text-processing" %}

## Videos

### Introduction and problem scenario

In the video, we talk about the applications of regex through the presentation
of a problem scenario.

{% include video.html id="1_mj549uhe" %}

### Introduction to `sed`

This video introduces `sed`, the famous *steam editor*.

{% include video.html id="1_dwoi2721" %}

### Basic `sed` commands

This video provides an overview of common `sed` commands.

{% include video.html id="1_4qf8p6w2" %}

### Bringing it together

This video shows how to use `sed` in order to solve the problem scenario
presented in the introduction.

{% include video.html id="1_x9k750ua" %}

### Introduction to `awk`

This video discusses the idea of keeping a *state* when parsing data in order to
expand on our earlier problem scenario, and presents `awk`.

{% include video.html id="1_rghu8cno" %}

### Basic `awk` usage

This video shows how we can call `awk` from the terminal, and how `awk` commands
are generally structured.

{% include video.html id="1_wc2acuf6" %}

### `awk` language constructs

Considering that `awk` is a programming language, the video shows how to
leverage common programming constructs.

{% include video.html id="1_gn5c82ou" %}

### Solution using `awk`

This video presents a solution to our expanded problem scenario using `awk`.

{% include video.html id="1_b5zi2cu6" %}

### Conclusion

This video provides a brief discussion of `sed`, `awk`, and `grep`, and wraps up
our topic on scripting.

{% include video.html id="1_sh2dcb28" %}
