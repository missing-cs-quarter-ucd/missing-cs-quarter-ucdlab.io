---
title: Regular expressions
---

## Table of contents
{:.no_toc}

* toc placeholder
{:toc}

## Slides

{% include slides.html id="regex" %}

## Videos

### Introduction to regular expressions (regex)

This video discusses how to think of regular expressions as a tool, and presents
a motivating problem scenario.

{% include video.html id="1_cli8pap6" %}

### Using regex 101

This video shows how to write basic regular expressions.

{% include video.html id="1_qkx2nz71" %}

### Using regex to extract data

This video shows how we can use regex not only to find data, but to extract
meaningful pieces of data out strings.

{% include video.html id="1_6mdhi8pv" %}

### Dialects of regex

This video aims to disambiguate different regex dialects in order to avoid
confusion.

{% include video.html id="1_650dt6gx" %}

### Demo: Enriching your experience with `grep`

This video is a demo showcasing the power of `grep` with full-on regular
expressions and its many useful flags.

{% include video.html id="1_19f3fjp2" %}

### The ubiquity of regex

This video concludes by claiming that regex is everywhere, along with brief
discussion of its limitations.

{% include video.html id="1_rojqp5pu" %}

