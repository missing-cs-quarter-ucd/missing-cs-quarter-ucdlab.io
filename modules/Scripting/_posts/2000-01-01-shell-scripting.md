---
title: Shell scripting
---

## Table of contents
{:.no_toc}

* toc placeholder
{:toc}

## Slides

{% include slides.html id="scripting" %}

*Note: The source code used in the slides and videos can be found in our [git
repository](https://gitlab.com/missing-cs-quarter-ucd/slides/-/tree/master/07.scripting/code).*

## Videos

### Introduction and shell scripting v1.0

This video discusses the benefits of shell scripting, and presents a first
attempt at shell scripting for renaming files.

{% include video.html id="1_ry1ipoo6" %}

### Bash language fundamentals

This video presents the syntax and main constructs of the Bash language:
e.g., variable assignments, string manipulations, arrays, and for-loops.

{% include video.html id="1_lxnpg26d" %}

### Shell scripting v2.0

This video shows a possible "throwaway" script to rename files en masse.

{% include video.html id="1_f391r88b" %}

### More Bash language features

This video presents more advanced features of the Bash language, such as exit
codes, conditionals, and functions.

{% include video.html id="1_1q52fhu4" %}

### Shell scripting v3.0

This video shows the breakdown of a scalable, reusable solution script for the
problem of file renaming.

{% include video.html id="1_5avi59v3" %}

### Good practices and conclusion

This video discusses how to use `shellcheck`, a bash script validator, concludes
the module.

{% include video.html id="1_r0gsdmzw" %}

