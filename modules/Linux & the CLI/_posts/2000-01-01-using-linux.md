---
title: Course introduction and using Linux
---

## Table of contents
{:.no_toc}

* toc placeholder
{:toc}

## Slides

{% include slides.html id="introduction" %}

## Videos

### Course introduction

This video introduces the reasons why this course is being offered and the
overarching goals this course aims to solve.

{% include video.html id="1_jnfq2gn6" %}

### Demo: Command-line interface

This video shows a few examples of what can be done when using the command-line
interface.

{% include video.html id="1_0dnspwyy" %}

### Unix history and usage

This video provides a brief history of the Unix operating system and its
prevalence in the industry.

{% include video.html id="1_xh0t244l" %}

### The package manager

This video shows a brief introduction to the package manager and its uses for
installing and removing software on Ubuntu.

{% include video.html id="1_8ittjz62" %}

## Extra resources

This video is a walkthrough tutorial on how to get setup using a virtual machine
to try out the Ubuntu 18.04 operating system. It was required in previous
offerings of this course, but now is just a potential resource for you to use.
M1 Macs cannot run Virtual Box.

{% include video.html id="1_wlkfchr4" %}

- [Virtualbox](https://www.virtualbox.org/) is a popular open-source machine
  emulator. It can be used to run a Linux machine on Windows for example.
- [Ubuntu 18.04](https://releases.ubuntu.com/18.04/) and [Lubuntu
  18.04](http://cdimage.ubuntu.com/lubuntu/releases/18.04/release/) are two
  popular Linux distributions.

Note that Ubuntu 18.04 is not the most recent Ubuntu release. As of April 2023,
the most recent Ubuntu release is [Ubuntu
22.04](https://releases.ubuntu.com/jammy/). The most recent Lubuntu release is
[Lubuntu 22.04](https://lubuntu.me/downloads/).
