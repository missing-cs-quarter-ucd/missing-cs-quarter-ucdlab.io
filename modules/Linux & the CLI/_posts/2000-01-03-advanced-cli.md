---
title: Advanced command line usage
---

## Table of contents
{:.no_toc}

* toc placeholder
{:toc}

## Slides

{% include slides.html id="cli-advanced" %}

## Videos

### Introduction to streams, redirection, and piping

This video introduces the concept of *IO streams*. Streams can be rerouted
to/from a file with *redirection* and between commands with *pipes*.

{% include video.html id="1_vqij2v1a" %}

### Common pipeline commands

This video introduces the commands `grep`, `cut`, `sort`, and `uniq`, which are
particularly helpful in piping.

{% include video.html id="1_b53nb6w9" %}

### Demo: Gradebook pipeline

This video goes through an example of constructing a pipeline to process a
gradebook. We see how to combine the commands introduced previously to determine
the frequency of assigned letter grades in the example data.

{% include video.html id="1_mh3h78e1" %}

### The `root` user and `sudo`

This video introduces the `root` user and the (possibly destructive) power it
has. Using super user abilities, this video also demonstrates how to modify
kernel parameters on-the-fly via the `sysfs`.

{% include video.html id="1_6npcdlbh" %}

### Exit codes

This video discusses process exit codes. Exit codes are usually used to indicate
the success or failure of a program. Using a special variable in the shell, we
can check the status of the previous command.

{% include video.html id="1_vjw8hhkp" %}


## Supplemental Topics

While helpful, the videos below are not required for the UC Davis course. We
encourage you to review them regardless.

### Interacting with jobs

This video introduces the concept of jobs in a terminal. Jobs can be run in the
foreground or background, and can be paused or stopped.

{% include video.html id="1_rdqvgmpy" %}


### Conditional execution, PATH, and SSH keys

This video briefly covers a few topics:

* Conditional execution can be used to run commands only if a previous command
  ran successfully or not.
* The PATH variable informs the shell of where to look for executable programs.
* SSH keys provide an alternative form of authentication for remote machines.

{% include video.html id="1_ybvxo7yq" %}

