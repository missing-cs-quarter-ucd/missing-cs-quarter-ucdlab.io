---
title: Using the GNU debugger
---

## Table of contents
{:.no_toc}

* toc placeholder
{:toc}

## Slides

{% include slides.html id="debugger" %}

## Videos

### Introduction & GDB setup

This video introduces the GNU DeBugger (GDB) and the steps required before
running it.

{% include video.html id="1_1zjeno6d" %}

### Using GDB and its use cases

This video describes how to interact with GDB via its *interactive shell*, and
the common use cases for the debugger.

{% include video.html id="1_sw9fgpao" %}

### Debugging segmentation faults

This video looks at two examples of programs with segmentation faults, and how
to use GDB to aid the debugging process.

{% include video.html id="1_0p9x2csp" %}

### Tracking bugs with breakpoints

This video presents a program exhibiting a *behavioral bug* and introduces the
concept of *breakpoints*.

{% include video.html id="1_avkojzi4" %}

### Printing and other userful commands

This video demonstrates the versatility of the print command and introduces a
few smaller, but helpful, commands.

{% include video.html id="1_768b6m33" %}

### Demo: debugging the Caesar cipher program

This video illustrates how to use GDB to aid in the debugging process for the
Caesar cipher program introduced in the previous module.

{% include video.html id="1_f3hesy4o" %}

### Conclusion

To wrap up the discussion about GDB, this video presents some of its limitations
and discusses the use of the software.

{% include video.html id="1_24x1q391" %}

