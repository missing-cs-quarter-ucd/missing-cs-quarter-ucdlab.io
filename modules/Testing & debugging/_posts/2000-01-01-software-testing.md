---
title: Software testing
---

## Table of contents
{:.no_toc}

* toc placeholder
{:toc}

## Slides

{% include slides.html id="testing" %}

*Note: The source code used in the slides and videos can be found in our [git
repository](https://gitlab.com/missing-cs-quarter-ucd/slides/-/tree/master/04.testing/code/usernames_example).*

## Videos

### Introduction to software testing

This video introduces the context for why software testing is important and some
of the approaches one can take to test their programs.

{% include video.html id="1_wyd0fodo" %}

### Demo: Writing tests for the username validator

This video introduces the username validator program and demonstrates how one
might create automated tests for it without the use of any third-party libraries
or frameworks.

{% include video.html id="1_gisrxudz" %}

This video presents a quick recap of the previous demo.

{% include video.html id="1_ry97cgcb" %}

### Introduction to unit-testing

This video introduces unit-testing, in which one unit-test checks the individual
components of a program as opposed to its end-to-end behavior.

{% include video.html id="1_5v15aji4" %}

### Writing unit tests

This video demonstrates writing unit tests for the username validator program
using the CUnit framework.

{% include video.html id="1_1ppql4uh" %}

### Test effectiveness

This video discusses the different approaches to managing developer resources
within the scope of testing. It also introduces the concept of code coverage.

{% include video.html id="1_6w15ks1m" %}

### Demo: Creating coverage reports

This video demonstrates how to generate a code coverage report for a C program
using `gcov` and `gcovr`, and how to analyze the generated report.

{% include video.html id="1_nfz3wnlc" %}

