---
title: Debugging methodologies
---

## Table of contents
{:.no_toc}

* toc placeholder
{:toc}

## Slides

{% include slides.html id="debugging" %}

*Note: The source code used in the slides and videos can be found in our [git
repository](https://gitlab.com/missing-cs-quarter-ucd/slides/-/tree/master/05.debugging/code).*

## Videos

### Introduction to debugging and common pitfalls

This video discusses the concept of debugging as problem solving and how it is
not typically taught in computer science courses. We address the common pitfalls
associated with debugging and possible mitigations.

{% include video.html id="1_qozrdfil" %}

### The MINS problem solving model

This video introduces MINS, a 4-step framework for debugging. The method is
demonstrated on a small application.

{% include video.html id="1_kb4lhbqx" %}

### Debugging the Caesar shift program

This video applies MINS to a faulty program implementing a Caesar shift--a
rudimentary cryptographic scheme for encoding and decoding secret messages.

{% include video.html id="1_c7mz5m8s" %}

