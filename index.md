---
layout: index
published: true
---

## Welcome!

In 2019, three student tutors at UC Davis recognized that there exists a gap in
knowledge between what is taught in introductory programming courses and what is
expected in later courses and industry internships.

The objective of this course is to bridge some of this gap. Inspired by the
issues we have observed as students and instructors, we will take a hands on
approach to learning about the tools and practices that make up a developers
toolbox!

## Course content

This course is divided into a total of ten modules, which are organized in four
main topics:

- I. Linux & the command-line interface (CLI)
	- 1/ Course introduction and using Linux
	- 2/ Introduction to the command line
	- 3/ Advanced command line usage
- II. Testing & debugging
	- 4/ Software testing
	- 5/ Debugging methodologies
	- 6/ Using the GNU debugger
- III. Scripting
	- 7/ Shell scripting
	- 8/ Regular expressions
	- 9/ Text processing (using `sed` & `awk`)
- IV. Version control
	- 10/ Version control (using `git`)

Each main topic can be accessed using the navigation bar above. Modules within a
unit can then be accessed using the menu in the left sidebar.

## Authors

This course was created by Grant Gilson, Stephen Ott, and Noah Rose Lesdesma,
with the assistance of Prof. Joël Porquet-Lupine and Aakash Prabhu.

