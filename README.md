# The Missing CS Quarter @ UC Davis

This repo contains the lecture materials for the student-led course *ECS 98F:
The Missing CS Quarter*, as given in the spring of 2021.

This course was created by Grant Gilson, Stephen Ott, and Noah Rose Lesdesma,
with the assistance of Prof. Joël Porquet-Lupine and Aakash Prabhu.

All of this work is shared under a [CC BY-NC-SA 4.0 International
License](https://creativecommons.org/licenses/by-nc-sa/4.0/).

Copyright © 2020-2021 The CS Missing Quarter

